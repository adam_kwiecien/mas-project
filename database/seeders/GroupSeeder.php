<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Group::factory()->count(5)->create();

        $user = User::query()->first();

        Group::all()->each(function (Group $group) use ($user) {
            $group->members()->attach($user);

            $randomUser = User::query()->inRandomOrder()->whereNot('id', $user->id)->first();
            $group->members()->attach($randomUser);
        });
    }
}
