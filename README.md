## Deployment

To deploy this project run

```bash
  composer install
  ./vendor/bin/sail artisan migrate --seed
```

## Requirements

**PHP:** 8.2

**docker** ^24

**docker-compose** ^3

## Features

- Add Post
    - add media
  - add group
    - add tags (optionally)
    - add image filters (optionally)
- See posts in groups that user is a member of
- Login
- Registration
- Sharing post with friends
    - share with message
    - selct friends to share to
- browswer session menagment
- password change (while logged in, there is no password recovery)
- account deletion
- profile information change
  - email 
  - name

## Test user credentials

`Email:` -> admin@admin.com
`Password` -> password

