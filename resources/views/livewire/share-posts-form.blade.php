<div>
    <form wire:submit.prevent="sendMessages" class="mx-6 mt-6 mb-6">

        <label for="message" class="block mb-2 text-sm font-medium text-gray-900">{{__('Content')}}</label>
        <textarea wire:model="content" name="content" placeholder="{{__('Write message to friends')}}" rows="4"
                  class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 "
                  placeholder="Write your thoughts here..."></textarea>
        @error('content') <span class="error">{{ $message }}</span> @enderror
        <br>
        <br>

        @foreach($friends as $friend)
            <div class="flex items-center mb-4">
                <input wire:model="selectedFriends" id="selectedFriends.{{$friend->id}}" type="checkbox" value="{{$friend->id}}"
                       class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                <label  for="selectedFriends.{{$friend->id}}"
                       class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                    {{$friend->name}}
                </label>
            </div>
        @endforeach
        @error('friends') <span class="error">{{ $message }}</span> @enderror
        <br>

        <button type="submit"
                class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            {{__('Send')}}
        </button>
    </form>
</div>
