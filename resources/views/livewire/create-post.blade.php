<div>
    <form wire:submit.prevent="saveContact" class="mx-6 mt-6 mb-6">

        <label for="message" class="block mb-2 text-sm font-medium text-gray-900">{{__('Content')}}</label>
        <textarea wire:model="content" name="content" placeholder="{{__('Write something about this post')}}" rows="4"
                  class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 "
                  placeholder="Write your thoughts here..."></textarea>
        @error('content') <span class="error">{{ $message }}</span> @enderror
        <br>
        <br>


        <label class="block mb-2 text-sm font-medium text-gray-900" for="file_input">{{__('Upload file')}}</label>
        <input wire:model="media" name="media"
               class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 focus:outline-none"
               type="file">
        @error('media') <span class="error">{{ $message }}</span> @enderror

        @if ($media)
            <img class="mx-auto w-1/2" src="{{ $media->temporaryUrl() }}">
        @endif<br>
        <br>


        <label for="tags" class="block mb-2 text-sm font-medium text-gray-900">{{__('Tags')}}</label>
        <select wire:model="tags" id="tags" multiple
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
            @foreach ($postTags as $tag)
                <option value="{{$tag->id}}">{{$tag->name}}</option>
            @endforeach
        </select>
        @error('tags') <span class="error">{{ $message }}</span> @enderror
        <br>
        <br>

        <label for="filter" class="block mb-2 text-sm font-medium text-gray-900">{{__('Filter')}}</label>
        <select wire:model="filter" id="tags"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
            <option>{{__('Select')}}</option>

            @foreach ($filters as $filter)
                <option value="{{$filter->value}}">{{$filter->getLabel()}}</option>
            @endforeach
        </select>
        @error('filter') <span class="error">{{ $message }}</span> @enderror
        <br>
        <br>


        <label for="filter" class="block mb-2 text-sm font-medium text-gray-900">{{__('Group')}}</label>
        <select wire:model="group" id="group"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
            @foreach ($groups as $group)
                <option value="{{$group->id}}">{{$group->name}}</option>
            @endforeach
        </select>
        @error('group') <span class="error">{{ $message }}</span> @enderror
        <br>

        <button type="submit"
                class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            {{__('Post')}}
        </button>

    </form>
</div>
