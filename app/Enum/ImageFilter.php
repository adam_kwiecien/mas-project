<?php

namespace App\Enum;

enum ImageFilter: string
{
    case BLUR = 'blur';
    case GREYSCALE = 'greyscale';
    case INVERT = 'invert';
    case BRIGHT = 'bright';
    case NONE = 'none';

    public function getLabel()
    {
        return match($this) {
            self::BLUR => 'Blur',
            self::GREYSCALE => 'Greyscale',
            self::INVERT => 'Invert',
            self::BRIGHT => 'Bright',
            self::NONE => 'None',

        };
    }
}
