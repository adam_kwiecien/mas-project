<?php

namespace App\Http\Controllers;

use App\Models\Post;

class MessageController
{
    public function create(Post $post)
    {
        return view('share-post', compact('post'));
    }
}
