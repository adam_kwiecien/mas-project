<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ShowPosts extends Component
{

    public function render()
    {
        $posts = Post::query()
            ->with('media', 'author', 'tags', 'group')
            ->where(function (Builder $builder) {
                return $builder->whereAuthorId(Auth::id())
                    ->orWhereHas('group.members', fn(Builder $builder) => $builder->whereUserId(Auth::id()));
            })
            ->paginate(5);


        return view('livewire.show-posts', compact('posts'));
    }
}
