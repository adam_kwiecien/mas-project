<?php

namespace App\Http\Livewire;

use App\Models\Media;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class SharePostsForm extends Component
{
    public $post;
    public $content;
    public $selectedFriends = [];

    protected $rules = [
        'content' => 'required|string',
        'selectedFriends.*' => 'exists:users,id|required',
    ];

    public function render()
    {
        $usersGroupsIds = Auth::user()->groups()->pluck('groups.id');

        $friends = User::query()
            ->whereHas('groups', fn(Builder $builder) => $builder->whereIn('groups.id', $usersGroupsIds))
            ->get();

        return view('livewire.share-posts-form', compact('friends'));
    }

    public function sendMessages()
    {
        $this->validate();

        $userId = Auth::id();

        foreach ($this->selectedFriends as $friendId) {
            Message::query()->create([
                'content' => $this->content,
                'sender_id' => $userId,
                'recipient_id' => $friendId,
            ]);

            /** @var Media $media */
            foreach ($this->post->media as $media)
                Message::query()->create([
                    'content' => Storage::disk($media->disc)->url($media->path),
                    'sender_id' => $userId,
                    'recipient_id' => $friendId,
                ]);

        }

        return redirect()->route('dashboard');
    }
}
