<?php

namespace App\Http\Livewire;

use App\Actions\PostService;
use App\Enum\ImageFilter;
use App\Http\Requests\PostRequest;
use App\Models\Group;
use App\Models\Tag;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreatePost extends Component
{
    use WithFileUploads;

    public $tags;
    public $content;
    public $media;
    public $filter;
    public $group;
    private PostService $postService;

    public function boot(PostService $postService)
    {
        $this->postService = $postService;
    }

    protected function getRules()
    {
        return (new PostRequest())->rules();
    }

    public function render(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('livewire.create-post', [
            'postTags' => Tag::all(),
            'groups' => Group::query()->whereHas('members', fn(Builder $builder) => $builder->where('users.id', Auth::id()))->get(),
            'filters' => ImageFilter::cases(),
        ]);
    }

    public function saveContact()
    {
        $this->validate();

        $post = $this->postService->make(
            $this->media->store('photos', 'public'),
            $this->content,
            $this->filter,
            $this->tags,
            $this->group
        );

        return redirect()->route('post.share', ['post' => $post]);
    }
}
