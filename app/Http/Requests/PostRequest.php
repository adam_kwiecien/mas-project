<?php

namespace App\Http\Requests;

use App\Enum\ImageFilter;
use App\Models\Group;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'tags.*' => ['required', 'exists:tags,id'],
            'content' => ['required', 'string'],
            'media' => ['required', 'image','max:8192'],
            'filter' => new Enum(ImageFilter::class),
            'group' => ['required', 'exists:tags,id', Rule::in($this->getGroupsIds())]
        ];
    }

    /**
     * @return mixed[]
     */
    private function getGroupsIds(): array
    {
        return Group::query()
            ->whereHas('members', fn(Builder $builder) => $builder->where('users.id', Auth::id()))
            ->pluck('id')->toArray();
    }
}
