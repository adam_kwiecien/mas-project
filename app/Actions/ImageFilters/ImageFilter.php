<?php

namespace App\Actions\ImageFilters;

use App\Enum\ImageFilter as FilterEnum;
use App\Models\Media;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageFilter
{
    public function filterImge(?FilterEnum $filter, Media $media): \Intervention\Image\Image
    {
        $path = Storage::disk($media->disc)->path($media->path);

        $image = Image::make($path)->save();

        $image = match ($filter) {
            FilterEnum::BLUR => $image->blur(30),
            FilterEnum::GREYSCALE => $image->greyscale(),
            FilterEnum::INVERT => $image->invert(),
            FilterEnum::BRIGHT => $image->brightness(50),
            FilterEnum::NONE => $image
        };

        return $image->save();
    }
}
