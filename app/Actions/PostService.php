<?php

namespace App\Actions;

use App\Enum\ImageFilter;
use App\Models\Media;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostService
{
    public function __construct(protected ImageFilters\ImageFilter $filterService)
    {
    }

    public function make(string $imagePath, string $content, string $filter, ?array $tags, int $groupId): Post
    {
        return DB::transaction(function () use ($groupId, $tags, $filter, $imagePath, $content) {
            $media = $this->createMediaWithFilter($imagePath, $filter);

            $post = $this->createPost($content, $groupId);

            $media->mediable()->associate($post)->save();

            if ($tags) {
                $post->tags()->attach($tags);
            }

            return $post;
        });
    }

    /**
     * @param string $imagePath
     * @param string $filter
     * @return Media|Builder|Model
     */
    private function createMediaWithFilter(string $imagePath, string $filter): Media|Builder|Model
    {
        $media = Media::query()->create([
            'path' => $imagePath,
            'disc' => 'public'
        ]);
        $this->filterService->filterImge(ImageFilter::tryFrom($filter), $media);
        return $media;
    }

    /**
     * @param string $content
     * @return Post
     */
    private function createPost(string $content, int $groupId): Post
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Post $post */
        $post = $user->posts()->create([
            'content' => $content,
            'group_id' => $groupId
        ]);

        return $post;
    }
}
